# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Record(models.Model):
    _inherit = "documents.folder"
    write_group = fields.Many2many('hr.employee','name', string='Write groups')
    read_group = fields.Many2many('hr.employee','work_phone', string='Read groups')


class subordinate(models.Model):
    _inherit = 'hr.employee'
    # subordinate = fields.Many2many('hr.employee', 'parent_id', string='subordinates')


    def _count(self):
        if len(self.subordinate_ids) != 0:
            for i in self.subordinate_ids:
                self.child = self.child + i
        else:
            self.child = self.child

    child = fields.Many2many('hr.employee', 'parent_id', string='subordinates', compute='_count')


